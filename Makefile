# Thanks
# * <http://solver.assistedcoding.eu/makefilegen>
# * <https://stackoverflow.com/a/1080180/522479>
OUTDIR	= build
OBJS	= $(OUTDIR)/main.o $(OUTDIR)/config.o $(OUTDIR)/redispermissionhandler.o
SOURCE	= main.cpp config.cpp redispermissionhandler.cpp
HEADER	= 
OUT		= $(OUTDIR)/vpermfs
CC		= g++
FLAGS	= -c -Wall `pkg-config fuse3 --cflags --libs` `pkg-config hiredis --cflags --libs`
FLAGS_DEBUG = -DDEBUG -g
FLAGS_RELEASE = -O3
LFLAGS	= `pkg-config fuse3 --cflags --libs` `pkg-config hiredis --cflags --libs`
LFLAGS_DEBUG = -DDEBUG -g
LFLAGS_RELEASE = -O3

all:
	@echo "run either 'make debug' or 'make release'"
	@exit 1

debug: FLAGS += $(FLAGS_DEBUG)
debug: LFLAGS += $(LFLAGS_DEBUG)
debug: $(OUT)

release: FLAGS += $(FLAGS_RELEASE)
release: LFLAGS += $(FLAGS_RELEASE)
release: $(OUT)
	strip $(OUT)

$(OUT): $(OBJS)
	$(CC) $(OBJS) $(LFLAGS) -o $(OUT)

$(OUTDIR)/main.o: main.cpp main.h
	$(CC) $(FLAGS) main.cpp -std=c++14 -o $(OUTDIR)/main.o

$(OUTDIR)/config.o: config.cpp config.h
	$(CC) $(FLAGS) config.cpp -std=c++14 -o $(OUTDIR)/config.o

$(OUTDIR)/redispermissionhandler.o: redispermissionhandler.cpp redispermissionhandler.h permissionhandler.h
	$(CC) $(FLAGS) redispermissionhandler.cpp -std=c++14 -o $(OUTDIR)/redispermissionhandler.o

clean:
	rm -f $(OBJS) $(OUT)

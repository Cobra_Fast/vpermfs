#ifndef PERMISSIONHANDLER_H
#define PERMISSIONHANDLER_H

#define FUSE_USE_VERSION 30

#include <string>

#include <fuse.h>
#include <sys/stat.h>
#include <unistd.h>

enum Permission
{
	Read = 1,
	Write = 2,
	Execute = 4,
	Search = 8
};

struct StatLite
{
	uid_t uid;
	gid_t gid;
	mode_t mode;

	StatLite()
	{
		this->uid = 0;
		this->gid = 0;
		this->mode = 0;
	}

	StatLite(uid_t uid, gid_t gid, mode_t mode)
	{
		this->uid = uid;
		this->gid = gid;
		this->mode = mode;
	}

	StatLite(struct StatLite *other)
	{
		this->uid = other->uid;
		this->gid = other->gid;
		this->mode = other->mode;
	}
};

class PermissionHandler
{
public:
	virtual bool Access(std::string path, mode_t requested_mode, struct fuse_context *fx) = 0;
	virtual bool AccessDirExec(std::string path, std::string rpath, struct fuse_context *fx) = 0;
	virtual bool Chmod(std::string path, mode_t mode, struct fuse_context *fx) = 0;
	virtual bool Chown(std::string path, uid_t uid, gid_t gid, struct fuse_context *fx) = 0;
	virtual bool Register(std::string path, struct StatLite st) = 0;
	virtual bool Rename(std::string opath, std::string npath) = 0;
	virtual bool Delete(std::string path) = 0;

	virtual bool Get(std::string path, struct StatLite &stat) = 0;
	virtual bool Set(std::string path, struct StatLite stat) = 0;

	static mode_t ConvertMode(enum Permission mode)
	{
		switch (mode)
		{
			case Read:
				return S_IRUSR | S_IRGRP | S_IROTH;
			case Write:
				return S_IWUSR | S_IWGRP | S_IWOTH;
			case Execute:
			case Search:
				return S_IXUSR | S_IXGRP | S_IXOTH;
			default:
				return 0;
		}
	}

	static mode_t ConvertAccessMode(int accessFlags)
	{
		mode_t mode = 0;
		if (accessFlags & R_OK)
			mode |= S_IRUSR | S_IRGRP | S_IROTH;
		if (accessFlags & W_OK)
			mode |= S_IWUSR | S_IWGRP | S_IWOTH;
		if (accessFlags & X_OK)
			mode |= S_IXUSR | S_IXGRP | S_IXOTH;
		return mode;
	}

	static void MergeStat(struct stat &st, struct StatLite stl)
	{
		st.st_uid = stl.uid;
		st.st_gid = stl.gid;
		st.st_mode &= ~(S_IRWXU | S_IRWXG | S_IRWXO);
		st.st_mode |= stl.mode;
	}

	static void MergeStat(struct stat *st, struct StatLite stl)
	{
		st->st_uid = stl.uid;
		st->st_gid = stl.gid;
		st->st_mode &= ~(S_IRWXU | S_IRWXG | S_IRWXO);
		st->st_mode |= stl.mode;
	}
};

#endif // ifndef PERMISSIONHANDLER_H

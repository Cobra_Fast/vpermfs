
#include <string>
#include <fstream>
#include <sstream>

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include "config.h"

config::config(std::string path)
{
	this->log_level = 0;
	this->redis_port = 0;

	this->parse(path);
}

void config::parse(std::string path)
{
	std::ifstream ifs(path);
	std::string line;
	std::string section("general");

	while (std::getline(ifs, line))
	{
		if (line.front() == '#')
			continue;

		
		if (line.front() == '[' && line.back() == ']')
		{
			char c_section[32];
			int sc = sscanf(line.c_str(), "[%[A-Za-z]]", c_section);
			if (sc == EOF)
				continue;
			section = std::string(c_section);
		}
		else
		{
			char c_key[32], c_value[1024];
			int sc = sscanf(line.c_str(), "%[^=]=%s", c_key, c_value);
			if (sc == EOF)
				continue;
			
			std::string key(c_key), value(c_value);

			if (section == "general")
			{
				if (key == "target")
					this->target = value;
				else if (key == "log")
					this->log = value;
				else if (key == "log_level")
					this->log_level = (char)atoi(c_value);
				else if (key == "name")
					this->name = value;
			}
			else if (section == "redis")
			{
				if (key == "socket")
					this->redis_socket = value;
				else if (key == "host")
					this->redis_host = value;
				else if (key == "port")
					this->redis_port = atoi(c_value);
				else if (key == "password")
					this->redis_password = value;
				else if (key == "cachesize")
					this->redis_cachesize = atoi(c_value);
			}
		}
	}
}
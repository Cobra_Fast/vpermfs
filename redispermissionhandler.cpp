#define FUSE_USE_VERSION 30

#include <mutex>
#include <string>

#include <errno.h>
#include <fuse.h>
#include <hiredis/hiredis.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>

#include "main.h"
#include "config.h"
#include "permissionhandler.h"
#include "redispermissionhandler.h"
#include "lrucache.h"

RedisPermissionHandler::RedisPermissionHandler(config *cfg)
{
	this->cfg = cfg;

	struct timeval timeout = {2, 500000}; // 2.5 sec
	if (!cfg->redis_socket.empty())
	{
		vpermfs_log(VPERMFS_LOG_MSG, "RedisPermissionHandler: connecting through socket '" + cfg->redis_socket + "'.", true);
		this->redis = redisConnectUnixWithTimeout(cfg->redis_socket.c_str(), timeout);
	}
	else
	{
		vpermfs_log(VPERMFS_LOG_MSG, "RedisPermissionHandler: connecting via " + cfg->redis_host + ":" + std::to_string(cfg->redis_port) + ".", true);
		this->redis = redisConnectWithTimeout(cfg->redis_host.c_str(), cfg->redis_port, timeout);
	}

	if (this->redis == NULL || this->redis->err)
	{
		if (this->redis)
			vpermfs_log(VPERMFS_LOG_ERROR, "Could not connect to Redis: " + std::string(this->redis->errstr), true);
		else
			vpermfs_log(VPERMFS_LOG_ERROR, "Could not connect to Redis. (Unknown error)", true);
		exit(1);
	}

	if (!cfg->redis_password.empty())
	{
		std::lock_guard<std::mutex> lg(this->redisLock);
		redisReply *reply = (redisReply*)redisCommand(this->redis, "AUTH %s", cfg->redis_password.c_str());
		if (reply->type == REDIS_REPLY_ERROR)
		{
			vpermfs_log(VPERMFS_LOG_ERROR, "Redis: " + std::string(this->redis->errstr), true);
			exit(1);
		}
		freeReplyObject(reply);
	}

	if (cfg->redis_cachesize > 0)
	{
		vpermfs_log(VPERMFS_LOG_MSG, "RedisPermissionHandler: using cache for " + std::to_string(cfg->redis_cachesize) + " items.", true);
		this->cache = new LruCache<std::string, struct StatLite>(cfg->redis_cachesize);
	}
	else
		this->cache = nullptr;
}

bool RedisPermissionHandler::Access(std::string path, mode_t requested_mode, struct fuse_context *fx)
{
	if (fx->uid == 0) // root
		return true;

	struct StatLite st;
	if (!this->Get(path, st))
	{
		st = StatLite(fx->uid, fx->gid, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
		this->Register(path, st);
	}

	mode_t omode = requested_mode & (S_IROTH | S_IWOTH | S_IXOTH);
	if ((st.mode & omode) == omode)
		return true;

	if (st.uid == fx->uid)
	{
		mode_t umode = requested_mode & (S_IRUSR | S_IWUSR | S_IXUSR);
		if ((st.mode & umode) == umode)
			return true;
	}

	struct passwd *pwd = getpwuid(fx->uid); // does not need free or delete
	if (!pwd) // can be NULL if user is not found
		return false;

	gid_t *groups = new gid_t[512];
	int ngroups = 512;
	// assert(user_gid == pwd->pw_gid); // Doesn't have to be but usually is.
	// getgrouplist includes the second argument in the list.
	// TODO So which gid do we need to pass into getgrouplist to get *all* groups?
	int rgroups = getgrouplist(pwd->pw_name, fx->gid, groups, &ngroups); // TODO cache this.
	if (rgroups == -1)
	{
		vpermfs_log(VPERMFS_LOG_ERROR, "getgrouplist(" + std::string(pwd->pw_name) + ", " + std::to_string(fx->gid) + ", ...) threw " + std::to_string(errno) + ".");
	}
	else
	{
		for (int i = 0; i < rgroups; i++)
		{
			if (st.gid == groups[i])
			{
				mode_t gmode = requested_mode & (S_IRGRP | S_IWGRP | S_IXGRP);
				if ((st.mode & gmode) == gmode)
					return true;
			}
		}
	}
	delete groups;

	return false;
}

bool RedisPermissionHandler::AccessDirExec(std::string path, std::string rpath, struct fuse_context *fx)
{
	std::string cur(path);
	while ((cur = getParentDirectoryPath(cur)) != "/")
	{
		if (!this->Access(cur, (S_IXUSR | S_IXGRP | S_IXOTH), fx))
			return false;
	}
	return true;
}

bool RedisPermissionHandler::Chmod(std::string path, mode_t mode, struct fuse_context *fx)
{
	struct StatLite st;
	if (!this->Get(path, st))
		return false;

	if (fx->uid == 0 || fx->uid == st.uid)
	{
		st.mode = mode;
		return this->Set(path, st);
	}

	return false;
}

bool RedisPermissionHandler::Chown(std::string path, uid_t uid, gid_t gid, struct fuse_context *fx)
{
	struct StatLite st;
	if (!this->Get(path, st))
		return false;

	if (fx->uid == 0) // Root can do anything.
	{
		if (uid < 4294967295)
			st.uid = uid;
		if (gid < 4294967295)
			st.gid = gid;
	}
	else // Users need to own the file and be a group member.
	{
		if (fx->uid != st.uid)
			return false;

		if (gid != st.gid && gid < 4294967295)
		{
			struct passwd *pwd = getpwuid(fx->uid); // does not need free or delete
			gid_t *groups = new gid_t[512];
			int ngroups = 512;
			int rgroups = getgrouplist(pwd->pw_name, fx->gid, groups, &ngroups);
			if (rgroups == -1)
			{
				vpermfs_log(VPERMFS_LOG_ERROR, "getgrouplist(" + std::string(pwd->pw_name) + ", " + std::to_string(fx->gid) + ", ...) threw " + std::to_string(errno) + ".");
			}
			else
			{
				for (int i = 0; i < rgroups; i++)
				{
					if (groups[i] == gid)
					{
						st.gid = gid;
						break;
					}
				}
				if (st.gid != gid) // Still different; user not in group.
					return false;
			}
			delete groups;
		}
	}
	
	bool res = this->Set(path, st);
	return res;
}

bool RedisPermissionHandler::Register(std::string path, struct StatLite st)
{
	return this->Set(path, st);
}

bool RedisPermissionHandler::Rename(std::string opath, std::string npath)
{
	std::lock_guard<std::mutex> lg(this->redisLock);
	redisReply *reply = (redisReply*)redisCommand(this->redis, "RENAME %b %b",
		opath.c_str(), opath.length(), npath.c_str(), npath.length());
	if (reply->type == REDIS_REPLY_ERROR)
	{
		vpermfs_log(VPERMFS_LOG_ERROR, "Redis: " + std::string(this->redis->errstr));
		return false;
	}
	freeReplyObject(reply);

	if (this->cache != nullptr)
		this->cache->Rename(opath, npath);

	return true;
}

bool RedisPermissionHandler::Delete(std::string path)
{
	std::lock_guard<std::mutex> lg(this->redisLock);
	redisReply *reply = (redisReply*)redisCommand(this->redis, "DEL %b", path.c_str(), path.length());
	if (reply->type == REDIS_REPLY_ERROR)
	{
		vpermfs_log(VPERMFS_LOG_ERROR, "Redis: " + std::string(this->redis->errstr));
		return false;
	}
	freeReplyObject(reply);

	if (this->cache != nullptr)
		this->cache->Remove(path);

	return true;
}

bool RedisPermissionHandler::Get(std::string path, struct StatLite &out)
{
	struct StatLite st;

	if (this->cache != nullptr && this->cache->TryGet(path, st))
	{
		out = st;
		return true;
	}
	else
	{	
		std::lock_guard<std::mutex> lg(this->redisLock);
		redisReply *reply = (redisReply*)redisCommand(this->redis, "GET %b", path.c_str(), path.length());
		
		if (reply->type == REDIS_REPLY_NIL)
			return false;
		else if (reply->type == REDIS_REPLY_ERROR)
		{
			vpermfs_log(VPERMFS_LOG_ERROR, "Redis: " + std::string(this->redis->errstr));
			return false;
		}
		else if (reply->len <= 0)
		{
			vpermfs_log(VPERMFS_LOG_WARNING, "Redis delivered 0 length value for '" + path + "'.");
			return false;
		}

		st = StatLite((struct StatLite*)reply->str);
		out = st;
		freeReplyObject(reply);

		if (this->cache != nullptr)
			this->cache->Set(path, st);

		return true;
	}
}

bool RedisPermissionHandler::Set(std::string path, StatLite stat)
{
	std::lock_guard<std::mutex> lg(this->redisLock);
	redisReply *reply = (redisReply*)redisCommand(this->redis,
		"SET %b %b",
		path.c_str(), (size_t)path.length(),
		&stat, (size_t)sizeof(stat));
	if (reply->type == REDIS_REPLY_ERROR)
	{
		vpermfs_log(VPERMFS_LOG_ERROR, "Redis: " + std::string(this->redis->errstr));
		return false;
	}
	freeReplyObject(reply);

	if (this->cache != nullptr)
		this->cache->Set(path, stat);

	return true;
}

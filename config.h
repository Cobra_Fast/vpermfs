#ifndef CONFIG_H
#define CONFIG_H

#include <string>

class config
{
public:
	std::string		name;
	std::string		target;
	std::string		log;
	char			log_level;
	
	std::string		redis_socket;
	std::string		redis_host;
	int				redis_port;
	std::string		redis_password;
	int				redis_cachesize;

	config(std::string path);
private:
	void parse(std::string path);
};

#endif // ifndef CONFIG_H

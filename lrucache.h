#ifndef LRUCACHE_H
#define LRUCACHE_H

#include <list>
#include <mutex>
#include <unordered_map>

template<typename TKey, typename TVal>
class LruCache
{
public:
	typedef typename std::pair<TKey, TVal> TKVP;
	typedef typename std::list<TKVP>::iterator ListIterator;

	LruCache(size_t _maxSize)
	{
		this->maxSize = _maxSize;
	}

	void Set(const TKey &key, const TVal &value)
	{
		std::lock_guard<std::mutex> lg(this->lock);

		auto previous = this->itemsMap.find(key);
		
		itemsList.push_front(TKVP(key, value));
		
		if (previous != itemsMap.end())
		{
			this->itemsList.erase(previous->second);
			this->itemsMap.erase(previous);
		}

		this->itemsMap[key] = this->itemsList.begin();

		if (this->itemsMap.size() > this->maxSize)
		{
			auto last = std::prev(this->itemsList.end());
			this->itemsMap.erase(last->first);
			this->itemsList.pop_back(); // Will change interators ("last" too).
		}
	}

	bool TryGet(const TKey &key, TVal &outval)
	{
		std::lock_guard<std::mutex> lg(this->lock);

		auto item = this->itemsMap.find(key);
		if (item != this->itemsMap.end())
		{
			this->itemsList.splice(this->itemsList.begin(), this->itemsList, item->second);
			outval = item->second->second;
			return true;
		}
		
		// outval = nullptr;
		return false;
	}

	bool ContainsKey(const TKey &key)
	{
		std::lock_guard<std::mutex> lg(this->lock);
		return this->itemsMap.find(key) != this->itemsMap.end();
	}

	void Remove(const TKey &key)
	{
		std::lock_guard<std::mutex> lg(this->lock);

		auto item = this->itemsMap.find(key);
		if (item != this->itemsMap.end())
		{
			this->itemsList.erase(item->second);
			this->itemsMap.erase(item);
		}
	}

	void Rename(const TKey &oldkey, const TKey &newkey)
	{
		TVal value;
		if (this->TryGet(oldkey, value))
		{
			this->Remove(oldkey);
			this->Set(newkey, value);
		}
	}

	size_t size() const
	{
		std::lock_guard<std::mutex> lg(this->lock);
		return this->itemsMap.size();
	}
private:
	std::list<TKVP> itemsList;
	std::unordered_map<TKey, ListIterator> itemsMap;
	std::mutex lock;
	size_t maxSize;
};

#endif // ifndef LRUCACHE_H
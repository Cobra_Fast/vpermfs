
#define FUSE_USE_VERSION 30

#include <cstdlib>
#include <cstring>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <mutex>
#include <string>

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/statvfs.h>

#include "main.h"
#include "config.h"
#include "permissionhandler.h"
#include "redispermissionhandler.h"

config *cfg;
PermissionHandler *permhandler;

std::ofstream logStream;
std::mutex log_m;

void vpermfs_log(char level, std::string message, bool write_to_cout)
{
	if (write_to_cout == true)
		std::cout << message << std::endl;
		
	if (logStream.is_open())
	{
		if (level > cfg->log_level)
			return;
		
		log_m.lock();
		
		auto t = std::time(nullptr);
		auto tm = *std::localtime(&t);
		
		logStream << std::put_time(&tm, "%F %T");
		logStream << " vpermfs [";
		logStream << cfg->name;
		logStream << "] ";
		
		if (level == 2)
			logStream << ": ";
		else if (level == 3)
			logStream << "(info): ";
		else if (level == 4)
			logStream << "(debug): ";
		else if (level == 1)
			logStream << "(warn): ";
		else if (level == 0)
			logStream << "(error): ";
		else
			logStream << "(unknown): ";
		
		logStream << message;
		logStream << std::endl;
		
		log_m.unlock();
	}
}

std::string getParentDirectoryPath(std::string path)
{
	auto lastSlash = path.find_last_of("/");
	if (lastSlash == 0)
		return "/";
	return path.substr(0, lastSlash);
}

std::string getRealPath(std::string path)
{
	if (path == "/")
		return cfg->target;
	return std::string(cfg->target + "/" + path);
}

static void* vpermfs_init(struct fuse_conn_info *fconn, struct fuse_config *fcfg)
{
	fcfg->entry_timeout = 0;
	fcfg->attr_timeout = 0;
	fcfg->negative_timeout = 0;

	return NULL;
}

static int vpermfs_getattr(const char *c_path, struct stat *stbuf, struct fuse_file_info *fi)
{
	// lstat needs search (+x) on the containing directory (and technically all directories up the hierarchy)
	std::string path(c_path);
	std::string rpath(getRealPath(path));
	if (path == "/")
		rpath = cfg->target;

	struct fuse_context *fx = fuse_get_context();
	if (!permhandler->AccessDirExec(path, rpath, fx))
		return -EACCES;

	if (lstat(rpath.c_str(), stbuf) != 0)
		return -errno;

	struct StatLite stl;
	if (permhandler->Get(path, stl))
		PermissionHandler::MergeStat(stbuf, stl);

	return 0;
}

static int vpermfs_access(const char *c_path, int mode)
{
	// access needs search (+x) on the containing directory (and technically all directories up the hierarchy)
	std::string path(c_path);
	std::string rpath(getRealPath(path));

	struct fuse_context *fx = fuse_get_context();
	if (!permhandler->AccessDirExec(path, rpath, fx))
		return -EACCES;

	if (mode == F_OK)
	{
		if (access(rpath.c_str(), mode) != 0)
			return -errno;
	}
	else
	{
		struct fuse_context *fx = fuse_get_context();
		bool res = permhandler->Access(path, PermissionHandler::ConvertAccessMode(mode), fx);
		if (!res)
			return -EACCES;
	}
	
	return 0;
}

static int vpermfs_readlink(const char *c_path, char *buf, size_t size)
{
	// readlink needs search (+x) on the containing directory (and technically all directories up the hierarchy)
	// because symlinks always have 0777.
	std::string path(c_path);
	std::string rpath(getRealPath(path));

	struct fuse_context *fx = fuse_get_context();
	if (!permhandler->AccessDirExec(path, rpath, fx))
		return -EACCES;

	int res = readlink(rpath.c_str(), buf, size - 1);
	if (res == -1)
		return -errno;
	buf[res] = '\0';
	return 0;
}

static int vpermfs_readdir(const char *c_path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi, enum fuse_readdir_flags flags)
{
	// opendir needs search (+x) on the containing directory (and technically all directories up the hierarchy)
	// lstat needs search (+x) on the containing directory (and technically all directories up the hierarchy)
	// readdir needs read (+r) on the containing directory
	std::string path(c_path);
	std::string rpath(getRealPath(path));

	struct fuse_context *fx = fuse_get_context();
	if (!permhandler->AccessDirExec(path, rpath, fx))
		return -EACCES;

	if (!permhandler->Access(path,
		PermissionHandler::ConvertMode(Permission::Read) | PermissionHandler::ConvertMode(Permission::Execute),
		fx))
		return -EACCES;

	struct dirent *de;

	DIR *dp = opendir(rpath.c_str());
	if (dp == NULL)
		return -errno;

	while ((de = readdir(dp)) != NULL)
	{
		struct stat st;
		std::string ipath(path + "/" + de->d_name);
		lstat(ipath.c_str(), &st);

		struct StatLite stl;
		if (permhandler->Get(path + "/" + de->d_name, stl))
			PermissionHandler::MergeStat(st, stl);

		filler(buf, de->d_name, &st, 0, (fuse_fill_dir_flags)0);
	}
	closedir(dp);
	return 0;
}

static int vpermfs_mkdir(const char *c_path, mode_t mode)
{
	// mkdir needs search and write (+wx) on the containing directory
	std::string path(c_path);
	std::string rpath(getRealPath(path));

	struct fuse_context *fx = fuse_get_context();
	if (!permhandler->AccessDirExec(path, rpath, fx))
		return -EACCES;
	
	if (!permhandler->Access(
		getParentDirectoryPath(path),
		PermissionHandler::ConvertMode(Permission::Write),
		fx))
		return -EACCES;

	if (mkdir(rpath.c_str(), mode) != 0)
		return -errno;

	permhandler->Register(path, StatLite(fx->uid, fx->gid, mode & ~fx->umask));

	return 0;
}

static int vpermfs_unlink(const char *c_path)
{
	// unlink needs search and write (+wx) on the containing directory
	std::string path(c_path);
	std::string rpath(getRealPath(path));

	struct fuse_context *fx = fuse_get_context();
	if (!permhandler->AccessDirExec(path, rpath, fx))
		return -EACCES;

	if (!permhandler->Access(
		getParentDirectoryPath(path),
		PermissionHandler::ConvertMode(Permission::Write),
		fx))
		return -EACCES;

	if (unlink(rpath.c_str()) != 0)
		return -errno;

	permhandler->Delete(path);

	return 0;
}

static int vpermfs_rmdir(const char *c_path)
{
	// rmdir needs search and write (+wx) on the containing directory
	std::string path(c_path);
	std::string rpath(getRealPath(path));

	struct fuse_context *fx = fuse_get_context();
	if (!permhandler->AccessDirExec(path, rpath, fx))
		return -EACCES;

	if (!permhandler->Access(
		getParentDirectoryPath(path),
		PermissionHandler::ConvertMode(Permission::Write),
		fx))
		return -EACCES;

	if (rmdir(rpath.c_str()) != 0)
		return -errno;

	permhandler->Delete(path);

	return 0;
}

static int vpermfs_symlink(const char *c_from, const char *c_to)
{
	return -ENOTSUP; // TODO

	// symlink needs search and write (+wx) on the containing directory
	std::string from_path(c_from);
	std::string from_rpath(getRealPath(from_path));

	std::string to_path(c_to);
	std::string to_rpath(getRealPath(to_path));

	struct fuse_context *fx = fuse_get_context();
	if (!permhandler->AccessDirExec(from_path, from_rpath, fx))
		return -EACCES;

	if (!permhandler->Access(
		getParentDirectoryPath(from_path),
		PermissionHandler::ConvertMode(Permission::Write),
		fx))
		return -EACCES;

	if (symlink(from_rpath.c_str(), to_rpath.c_str()) != 0)
		return -errno;
	return 0;
}

static int vpermfs_rename(const char *c_from, const char *c_to, unsigned int flags)
{
	// rename needs search and write (+wx) on both containing directories
	std::string from_path(c_from);
	std::string from_rpath(getRealPath(from_path));
	std::string from_pdpath(getParentDirectoryPath(from_path));

	std::string to_path(c_to);
	std::string to_rpath(getRealPath(to_path));
	std::string to_pdpath(getParentDirectoryPath(to_path));

	if (flags > 0)
		return -EINVAL;

	struct fuse_context *fx = fuse_get_context();
	if (!permhandler->AccessDirExec(from_path, from_rpath, fx))
		return -EACCES;

	if (!permhandler->Access(
		from_pdpath,
		PermissionHandler::ConvertMode(Permission::Write),
		fx))
		return -EACCES;

	if (from_pdpath != to_pdpath)
	{
		if (!permhandler->AccessDirExec(to_path, to_rpath, fx))
			return -EACCES;

		if (!permhandler->Access(
			to_pdpath,
			PermissionHandler::ConvertMode(Permission::Write),
			fx))
			return -EACCES;
	}

	if (rename(from_rpath.c_str(), to_rpath.c_str()) != 0)
		return -errno;

	permhandler->Rename(from_path, to_path);

	return 0;
}

static int vpermfs_link(const char *c_from, const char *c_to)
{
	// unknown permission requirements
	std::string from_path(c_from);
	std::string from_rpath(getRealPath(from_path));

	std::string to_path(to_path);
	std::string to_rpath(getRealPath(to_path));

	struct fuse_context *fx = fuse_get_context();
	if (fx->uid != 0) // TODO find out what the actual permission requirements are.
		return -EACCES;

	if (link(from_rpath.c_str(), to_rpath.c_str()) != 0)
		return -errno;
	return 0;
}

static int vpermfs_chmod(const char *c_path, mode_t mode, struct fuse_file_info *fi)
{
	// chmod only allows the owner of an object (or root) to change permissions
	// <https://unix.stackexchange.com/a/229553/45678>
	std::string path(c_path);
	std::string rpath(getRealPath(path));

	struct fuse_context *fx = fuse_get_context();
	if (!permhandler->Chmod(path, mode, fx))
		return -EACCES;

	// We are enforcing virtual permissions, the actual permission remain unchanged.
	// if (chmod(rpath.c_str(), mode) != 0)
	//  	return -errno;
	return 0;
}

static int vpermfs_chown(const char *c_path, uid_t uid, gid_t gid, struct fuse_file_info *fi)
{
	// chown needs root, chgrp needs ownership of the file and membership in the desired group
	// <https://unix.stackexchange.com/a/229553/45678>
	std::string path(c_path);
	std::string rpath(getRealPath(path));

	struct fuse_context *fx = fuse_get_context();
	if (!permhandler->Chown(path, uid, gid, fx))
		return -EACCES;

	// We are enforcing virtual permissions, the actual permission remain unchanged.
	// if (lchown(rpath.c_str(), uid, gid) != 0)
	// 	return -errno;
	return 0;
}

static int vpermfs_truncate(const char *c_path, off_t size, struct fuse_file_info *fi)
{
	// truncate needs write (+w) on the file and search (+x) on the containing directory
	int res;
	if (fi != NULL)
		res = ftruncate(fi->fh, size);
	else
	{
		std::string path(c_path);
		std::string rpath(getRealPath(path));

		struct fuse_context *fx = fuse_get_context();
		if (!permhandler->AccessDirExec(path, rpath, fx))
			return -EACCES;

		if (!permhandler->Access(
			path,
			PermissionHandler::ConvertMode(Permission::Write),
			fx))
			return -EACCES;

		res = truncate(rpath.c_str(), size);
	}

	if (res == -1)
		return -errno;
	return 0;
}

static int vpermfs_create(const char *c_path, mode_t mode, struct fuse_file_info *fi)
{
	// open needs search (+x) on the containing directory,
	// open needs desired modes on the file if it exists,
	// open needs write (+w) on the containing directory if the file does not exist.
	// mode option O_TRUNC implies write (+w) permission on an existing file.
	std::string path(c_path);
	std::string rpath(getRealPath(path));

	struct fuse_context *fx = fuse_get_context();
	if (!permhandler->AccessDirExec(path, rpath, fx))
		return -EACCES;

	bool fexists = true;
	struct stat st;
	if (lstat(rpath.c_str(), &st) == -1 && errno == ENOENT) // File does not exist.
	{
		fexists = false;
		if (!permhandler->Access(
			getParentDirectoryPath(path),
			PermissionHandler::ConvertMode(Permission::Write),
			fx))
			return -EACCES;
	}
	else // File exists (or other error).
	{
		if (fi->flags & (O_RDONLY | O_RDWR))
			if (!permhandler->Access(path, PermissionHandler::ConvertMode(Permission::Read), fx))
				return -EACCES;
		if (fi->flags & (O_TRUNC | O_WRONLY | O_RDWR))
			if (!permhandler->Access(path, PermissionHandler::ConvertMode(Permission::Write), fx))
				return -EACCES;
	}

	int res = open(rpath.c_str(), fi->flags, mode);
	if (res == -1)
		return -errno;
	fi->fh = res;

	if (fexists == false) // File was created.
		permhandler->Register(path, StatLite(fx->uid, fx->gid, mode & ~fx->umask));

	return 0;
}

static int vpermfs_open(const char *c_path, struct fuse_file_info *fi)
{
	// see vpermfs_create for required permissions for open
	std::string path(c_path);
	std::string rpath(getRealPath(path));

	struct fuse_context *fx = fuse_get_context();
	if (!permhandler->AccessDirExec(path, rpath, fx))
		return -EACCES;

	bool fexists = true;
	struct stat st;
	if (lstat(rpath.c_str(), &st) == -1 && errno == ENOENT) // File does not exist.
	{
		fexists = false;
		if (!permhandler->Access(
			getParentDirectoryPath(path),
			PermissionHandler::ConvertMode(Permission::Write),
			fx))
			return -EACCES;
	}
	else // File exists (or other error).
	{
		if (fi->flags & (O_RDONLY | O_RDWR))
			if (!permhandler->Access(path, PermissionHandler::ConvertMode(Permission::Read), fx))
				return -EACCES;
		if (fi->flags & (O_TRUNC | O_WRONLY | O_RDWR))
			if (!permhandler->Access(path, PermissionHandler::ConvertMode(Permission::Write), fx))
				return -EACCES;
	}

	int res = open(rpath.c_str(), fi->flags);
	if (res == -1)
		return -errno;
	fi->fh = res;

	if (fexists == false) // File was created.
		permhandler->Register(path, StatLite(fx->uid, fx->gid, ~fx->umask));

	return 0;
}

static int vpermfs_read(const char *c_path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
	// read does not require any permissions, they are enforced by open()
	// see vpermfs_create for required permissions for open
	int fd, res;

	if (fi == NULL)
		return -EINVAL;
	else
		fd = fi->fh;

	if (fd == -1)
		return -errno;
	
	res = pread(fd, buf, size, offset);
	if (res == -1)
		return -errno;

	return res;
}

static int vpermfs_write(const char *c_path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
	// write does not require any permissions, they are enforced by open()
	// see vpermfs_create for required permissions for open
	int fd, res;

	if (fi == NULL)
		return -EINVAL;
	else
		fd = fi->fh;

	if (fd == -1)
		return -errno;

	res = pwrite(fd, buf, size, offset);
	if (res == -1)
		return -errno;

	return res;
}

static int vpermfs_statfs(const char *c_path, struct statvfs *stbuf)
{
	// statvfs needs search (+x) on the directory containing the mountpoint
	// this should not need explicit enforcement here
	if (statvfs(cfg->target.c_str(), stbuf) == -1)
		return -errno;
	return 0;
}

static int vpermfs_release(const char *c_path, struct fuse_file_info *fi)
{
	// close does not require any permissions, they are enforced by open()
	close(fi->fh);
	return 0;
}

static int vpermfs_fsync(const char *c_path, int isdatasync, struct fuse_file_info *fi)
{
	// fsync does not require any permissions, they are enforced by open()
	if (fsync(fi->fh) == -1)
		return -errno;
	return 0;
}

static int vpermfs_fallocate(const char *c_path, int mode, off_t offset, off_t length, struct fuse_file_info *fi)
{
	// posix_fallocate does not require any permissions, they are enforced by open()
	// see vpermfs_create for required permissions for open
	if (mode > 0)
		return -EOPNOTSUPP;

	int fd;

	if (fi == NULL)
		return -EINVAL;
	else
		fd = fi->fh;

	if (fd == -1)
		return -errno;

	int res = posix_fallocate(fd, offset, length);

	return -res;
}

static int vpermfs_utimens(const char *c_path, const struct timespec ts[2], struct fuse_file_info *fi)
{
	auto timespectotimeval = [](struct timespec ts)
	{
		struct timeval tv;
		
		if (ts.tv_nsec == UTIME_NOW)
		{
			gettimeofday(&tv, NULL);
			return tv;
		}
		
		if (ts.tv_nsec == UTIME_OMIT) // this needs to be handled outside of this function
		{
			tv.tv_usec = tv.tv_sec = 0;
			return tv;
		}
		
		tv.tv_sec = ts.tv_sec;
		tv.tv_usec = ts.tv_nsec / 1000;
		
		while (tv.tv_usec >= 1000000)
		{
			tv.tv_sec += 1;
			tv.tv_usec -= 1000000;
		}
		
		return tv;
	};

	std::string path(c_path);
	std::string rpath(getRealPath(path));

	struct fuse_context *fx = fuse_get_context();
	if (!permhandler->AccessDirExec(path, rpath, fx))
		return -EACCES;

	if (ts[0].tv_nsec == UTIME_OMIT || ts[1].tv_nsec == UTIME_OMIT)
	{
		struct stat st;
		if (lstat(rpath.c_str(), &st) == -1)
			return -errno;

		if (fx->uid != 0 && fx->uid != st.st_uid
			&& !permhandler->Access(path, PermissionHandler::ConvertMode(Permission::Write), fx))
			return -EACCES;

		struct timeval tv[2];
		if (ts[0].tv_nsec == UTIME_OMIT)
			tv[0] = timespectotimeval(st.st_atim);

		if (ts[1].tv_nsec == UTIME_OMIT)
			tv[1] = timespectotimeval(st.st_mtim);

		if (lutimes(rpath.c_str(), tv) == -1)
			return -errno;
	}

	return 0;
}

struct vpermfs_fuse_operations : fuse_operations
{
	vpermfs_fuse_operations()
	{
		this->init = vpermfs_init;
		this->getattr = vpermfs_getattr;
		this->access = vpermfs_access;
		this->readlink = vpermfs_readlink;
		this->readdir = vpermfs_readdir;
		this->mkdir = vpermfs_mkdir;
		this->symlink = vpermfs_symlink;
		this->unlink = vpermfs_unlink;
		this->rmdir = vpermfs_rmdir;
		this->rename = vpermfs_rename;
		this->link = vpermfs_link;
		this->chmod = vpermfs_chmod;
		this->chown = vpermfs_chown;
		this->truncate = vpermfs_truncate;
		this->open = vpermfs_open;
		this->create = vpermfs_create;
		this->read = vpermfs_read;
		this->write = vpermfs_write;
		this->statfs = vpermfs_statfs;
		this->release = vpermfs_release;
		this->fsync = vpermfs_fsync;
		this->fallocate = vpermfs_fallocate;
		this->utimens = vpermfs_utimens;
	}
};

static struct vpermfs_fuse_operations vpermfs_oper;

int main (int argc, char** argv)
{
	cfg = new config(argv[1]);

	if (!cfg->log.empty())
		logStream.open(cfg->log, std::ios_base::app);

	vpermfs_log(VPERMFS_LOG_MSG, "Target: " + cfg->target, true);

	permhandler = new RedisPermissionHandler(cfg);

	for (int i = 1; i < argc; i++)
		argv[i] = argv[i + 1];
	argc--;

	return fuse_main_real(argc, argv, &vpermfs_oper, sizeof(fuse_operations), NULL);
}

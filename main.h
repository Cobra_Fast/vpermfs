#ifndef MAIN_H
#define MAIN_H

#include <string>

#include "config.h"
#include "permissionhandler.h"

#define VPERMFS_LOG_ERROR	0
#define VPERMFS_LOG_WARNING	1
#define VPERMFS_LOG_MSG		2
#define VPERMFS_LOG_INFO	3
#define VPERMFS_LOG_DEBUG	4

extern config *cfg;
extern PermissionHandler *permhandler;

extern void vpermfs_log(char level, std::string message, bool write_to_cout = false);
extern std::string getRealPath(std::string path);
extern std::string getParentDirectoryPath(std::string path);

#endif // ifdef MAIN_H
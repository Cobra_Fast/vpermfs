#ifndef REDISPERMISSIONHANDLER_H
#define REDISPERMISSIONHANDLER_H

#define FUSE_USE_VERSION 30

#include <mutex>
#include <string>

#include <fuse.h>
#include <hiredis/hiredis.h>
#include <sys/stat.h>
#include <unistd.h>

#include "config.h"
#include "permissionhandler.h"
#include "lrucache.h"

class RedisPermissionHandler : public PermissionHandler
{
public:
	bool Access(std::string path, mode_t requested_mode, struct fuse_context *fx);
	bool AccessDirExec(std::string path, std::string rpath, struct fuse_context *fx);
	bool Chmod(std::string path, mode_t mode, struct fuse_context *fx);
	bool Chown(std::string path, uid_t uid, gid_t gid, struct fuse_context *fx);
	bool Register(std::string path, struct StatLite st);
	virtual bool Rename(std::string opath, std::string npath);
	bool Delete(std::string path);
	
	bool Get(std::string path, struct StatLite &stat);
	bool Set(std::string path, struct StatLite stat);

	RedisPermissionHandler(config *cfg);
private:
	redisContext *redis;
	config *cfg;
	std::mutex redisLock;
	LruCache<std::string, struct StatLite> *cache;
};

#endif // ifndef REDISPERMISSIONHANDLER_H

# vpermfs
Keep track of and enforce "virtual" permissions on top of another filesystem.

### Motivation
This FUSE filesystem was written out of the motivation to properly allow multiple users to use a single sshfs mountpoint with `allow_other`. sshfs does not enforce permissions beyond what happens on the remote host, meaning no matter who uses the mountpoint, files are going to come out with the user and permissions of the remote user.

With vpermfs, vpermfs uses `allow_other` instead of sshfs. Users can then use the vpermfs mount as they normally would any other directory or mount. Mounting both as root (or some disabled user) alleviates risk of unchecked access to the underlying sshfs.

### Implementation
The implementation currently uses Redis to associate simplified `stat` structs with their FUSE-absolute path. Redis provides a simple and well performing key-value store with options for data permanence.

#### Limitations
vpermfs currently does not automatically create stat entries for files and directories that were created outside of vpermfs. A workaround for adding such files and directories is to use `touch` on them. Whoever touches such a file first will own it and safe permissions of `0755` (`rwxr-xr-x`) will be applied.

### Configuration
A simple configuration file is required.  
It uses a simple syntax and does **not** allow for extraneous whitespace. Comment lines start with `#`.

#### Example
```
[general]
# The name will appear as a prefix in logs. Currently has no other effects.
name=Whatever
target=/underlying/filesystem/or/directory
log=/var/log/vpermfs.log
# Will only log messages of equal or lower level.
# Levels: 0=error, 1=warning, 2=messages, 3=extra info, 4=debug
log_level=5

[redis]
socket=/run/redis.sock
# when socket= is set, host= and port= are ignored.
host=127.0.0.1
port=12345
# password= is optional.
# When set AUTH will be executed immediately.
password=hunter2
# cachesize= is optional.
# Max number of stat structs to keep in vpermfs' memory.
# Increases performance by reducing communication with Redis.
cachesize=64
```

### Usage
Using vpermfs with `-o allow_other` is strongly recommended. Not doing so will work but defeat the purpose.

`vpermfs /your/config/file [fuse options] /mountpoint`  
e.g. `vpermfs /etc/vpermfs.conf -o allow_other /mnt/safe-remote`

`-f` (foreground/no-fork) is a useful fuse option for systemd units.
